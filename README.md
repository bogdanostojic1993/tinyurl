### _To run the project please read the README.md inside the submodules_
---

## Questions
- _What is a URL shortening system_ :
    - A URL shortening system is a service that converts long URLs into shorter ones.
---
- _What's the main value? Who needs such a system and why?_ :
    - The main value is that it is more convenient. Short URLs look more pleasing, trustworthy, and in some use cases when there is a limit in the number of characters it is the only option, like Twitter or SMS messages.
---
- _Describe The main mechanism of work and system components._ :
    - The main mechanism is a server that redirects the user from the short URL to the URL that it really wanted to be sent. When you shorten a URL you store the real URL in a database with fields for the long or URL that is going to be redirected to. Then when you visit a link with the short URL, the server does a lookup in the database if it has that URL stored, if it does it redirects you to the real URL.
---
- _What do you think are the main challenges in implementing and running the system_ :
    - I think the database will be getting hit hard for reads and writes, so put in some read/write replicas when the need comes and implement a caching layer infront of it with Redis
---
- _Try to suggest some ideas for advanced features._ :  
    - Branded links, where people can change the top level domain with their custom one. 
---


_I had little experience with Angular and almost zero with Typescript before I started the task. That took up 80% of my time on the project, so apologies for the project not having structure or not looking too good._